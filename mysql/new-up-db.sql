-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.17 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ravtech
CREATE DATABASE IF NOT EXISTS `ravtech`;
USE `ravtech`;

-- Dumping structure for table ravtech.user_profile
CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isadmin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table ravtech.user_profile: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` (`id`, `first_name`, `last_name`, `email`, `password`, `isadmin`) VALUES
  (1, 'elchanan', 'solberg', 'elchanan@ravtech.co.il', '$2b$10$4u6i6NbyYiohrtzCeDmew.AyIYiCFtym2Fnjr7Kc4oR4QoMkAsaQ2', 1),
	(2, 'boaz', 'zinger', 'zingerboaz@gmail.com', '$2b$10$4u6i6NbyYiohrtzCeDmew.AyIYiCFtym2Fnjr7Kc4oR4QoMkAsaQ2', 1);
-- end
